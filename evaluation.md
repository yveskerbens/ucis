# Systems Evaluation



# Agenda

- Qualitative Evaluation
	- Cognitive Walkthrough
	- Heuristic Analysis
	- Controlled Experiments

- Usability Studies

- A/B Testing

- Case Study



### Qualitative Evaluation
- General Overview
  - Qualitative Evaluation: Sense of Usability

  - Quantitative evaluation =/= Qualitative Evaluation

Note:
- Qualitative Evaluation Is more design to give you  a sense of usability
  or to put you in the place of the user

- The objectives of the Quantitative Evaluation is to evaluate system or prototype of system

- There  are two classes of evaluation:
    - Quantitative evaluation and qualitative evaluation
    - Quantitative evaluation contrast with qualitative evaluation where we are taking measurement
    (We saw this last week with concept such as: Speed, accuracy, learnability, memorability and user's preferences) and collecting numbers so that we can do numerical, mathematical analysis


## Qualitative Evaluation Techniques

- Cognigive Walktrough

- Heuristic Analysis

- Personas

- A/B Testing


### Cognitive Walkthrough
- Requirements
    - Description or prototype of interface
    - Task description
    - List of actions to complete task
    - User background

Note:
- The items in Cognitive Walkthrough can be done with:
  - an interface or
  - a prototype of an interface

- Task and actions
  - You come up with a list of actions
- User background: need to develop an idea of the user background


### Prototype
- Gesture based authentication system prototype
  -  Cognitive Walkthrough objectives

    - Whether users know how to perform actions

- paper based prototype


### Prototype (Cont')

![External Image](http://172.16.52.253:8000/plugin/markdown/img/chap3/phoneexample1.jpg)


![External Image](https://gitlab.com/yveskerbens/ucis/blob/master/img/chap3/phoneexample1.jpg)


![External Image](http://172.16.52.253:8000/plugin/markdown/img/chap3/phoneexample2.jpg)


### Prototype (Cont')
- Task description:
    - authenticating  & login

    - Enter One's distinctive gesture

-  Action: user must input gesture
    - Click the ON Button

- User background: Tech savvy & Non-tech savvy users
Note:
- Requirements
    - Description or prototype of interface: A phone login systme
    - Task description: user need to authenticate to login 
    - List of actions to complete task:
		- input gesture
    - User background: Prety general set of users


### What we look for: 
- Will users know how to perform the action?

- Will users see the control?

- Will users know the control does what they want?

- Will users understand the feedback?


![External Image](http://172.16.52.253:8000/plugin/markdown/img/chap3/phoneexample3.png)


### Heuristic Analysis
- Follow "Rules of thumb" or Suggestions about good design

- Can be done by experts/designers, fast and easy

- May miss problems users would catch

Note:
- When you do Heuristic analysis you follow rules of thumb  or suggestions about good design
You go through  the interface with a set of what you think are representative task or what users
would do and then check whether  these rules are follow


###  Heuristic Analysis' Rules

- Nielsen's Heuristic

	- Simple and natural dialog
	- Speaker the user's language
	- Minimizing the user's memory load
	- Consistency
	- Giving good feedback


###  Heuristic Analysis' Rules (Cont')
- Nielsen's Heuristic
	- Clearly marked  exits
	- Shortcuts
	- Preventing errors
	- Good errors message
	- Providing helps and documentation


### Heuristic Analysis  example 
![External Image](http://172.16.52.253:8000/plugin/markdown/img/chap3/phoneexample4.jpg)


### Qualitative evaluation: Personas

- Fictitious user representing a class of users

- Reference point for design and analysis

- Goals or goal to accomplish

Note:
- This method consist of coming with user (Fictitious) representing class of users
these Fictitious users then become Reference point for any design and analysis work

- When designing a system you to ask who the users are and look at this system
and ask whether the  persona can use that system
question whether the system meets his goal


### Example of Personna Cont'

![External Image](http://172.16.52.253:8000/plugin/markdown/img/chap3/personna1.jpg)


![External Image](http://172.16.52.253:8000/plugin/markdown/img/chap3/phoneexample5.jpg)


### Conclusions
- Qualitative evaluation provide insights into the system usability 
- various level of complexity
- can be quick and inexpensive, but may miss insights users provide
- various level of complexity
 
Note:
- Qualitative evaluation can provide insights into the usability of a system without measurement and timing
- various level of complexity
     - Heuristic evaluation pretty straightforward
     - Development of good persona  is a time consuming process



### Controlled Experiments

Note:
Running controlled experiments is a great thing that designer needs to do
if they are trying to analyze the usability of a system


### Controlled Experiments Processes
- State a lucid, testable hypothesis
- Identify independent and dependent variables
- Design experimental protocol
- Design experimental protocol
- Running some pilot participants


### Controlled Experiments Processes 
- Fix the experimental protocol
- Run the experiment
- Perform statistical analysis
- Draw Conclusions
- Communicating  results

Note:
This is a scientific methodology
- State a lucid, testable hypothesis:
  The process does start by testing and lucid, testable hypothesis

- Identify independent and dependent variables
  The variables are things that are going to be modified and measured

- Design experimental protocol
  The protocol is the actual steps to be used to carry on the hypothesis

- Design experimental protocol
  Hopefully the people that are going to use the system. In case real users are not
  available person with similar experience can be chosen.

- Perform statistical analysis
 - after collecting data from participants, this step is performed


###  Example: Mobile phone logging
![External Image](http://172.16.52.253:8000/plugin/markdown/img/chap3/controlled1.png)


STEP1:  State a lucid, testable hypothesis

 Mobile phone login with fingerprint  is faster than with PIN entry


###   Choose the variables: example cont's

- Manipulate one or more independent variables
    - Login Method

- Observe effect on one or more dependent variables
    - Time to login

Note:
- The independent variables are thing that you change or Manipulate
-  dependent variables are things that you measure

 A list of both is needed


###  Design experimental protocol
- Choose tasks

- Between or within subjects?
    - Between subjects: each subjects run one condition

    - Within subjects: each subjects runs several conditions

Note:
The experimental protocol
	- They are actually things that people will go through when doing 
	   their expirement
	- The experimental protocol is a mandatory step when during measurement
	   Choose firstly the tasks (clear concrete things that users will do)

- If we do a Between experiment for the phone loging 
	we would typically have half of the people loging with passcode 
	and the other half with fingerpring


###  Design experimental protocol example
![External Image](http://172.16.52.253:8000/plugin/markdown/img/chap3/controlled2.png)


### Run the experiment
-  Run a pilot study!

-  Have a check list of steps

-  Collect data

Note:
- Have a check list of steps (you need the steps to be the same for all users)
	- You should literally have  list giving the same set of instruction to everyone
- Collect data
	- After getting everyone to do the experiment you collect data for analysis purpose 


### Analysis
- statistical analysis (eg t-test)

- Report results
 ![External Image](http://172.16.52.253:8000/plugin/markdown/img/chap3/ttest1.png)

Note:
t-test: standard statistical test



### Usability Studies


### What is it?

- Analyze how useable a system is

Note: 
- This is a fundamental part of analyzing the usability of a system,
  Whether it's a security system, a website or a mobile application

- Run  a usability study to judge how an interface facilitates tasks 
  with respect to the aspects of usability mentioned earlier
- Aspect of usability studies: Speed, efficiency, learnability, memorability, 
  user preferences (we discussed these aspects earliers

- We can run these tasks by having users sitting in front of a system 
run the tests, then collecting data for analysis


### Testing usability of security
- Security NOT a so frequent task

- Good security is seamless part of task

Note:
- when testing usability of security it's important to get in mind security is rarely the task users set
  out to accomplish

- Security should be part of the background


### Usability Study Process

- Define task (and their importance)

- Develop Questionnaires

Note:
-  Define task (and their importance)
  - Find out most obvious things that users are trying to do in a system

- Develop Questionnaires
  	Used to collect data a time. Can ask people about their opinion in the Questionnaires
	asking people for their feedback on thing they did like or did not


### Selecting Tasks
- Present as a task not a question

- Be specific

- Don't be vague or provide tiny insignificant tasks

- Choose representative tasks 

Note:
- Good task vs Bad task
	1- Create an itinerary between Boston and los Angeles
	2- How many flight are available between ... ?

- Be specific
	- users should not have to figure out what they are asked to get done

- Choose representative tasks
	- Choose representative tasks that reflect the most important things a user would do with the interface


###  Security Tasks

- Security is almost never a task
- Good task for banking website
    - Check balance
    - Make transfer

- Bad task for  banking website
  - Login to your account

Note: 
- Security tasks should be running in normal flow of tasks that people are doing

### Pre-Test Questionnaires
- Learn any relevant background about the subjects


###  Post-Test Questionnaires

- Have user provide feedback on the interface


### Evaluation

- Users are given a list of tasks and asked to perform each task

-  Interaction with user is governed by different protocol

Note: user's interaction is governed by some different way of observing  the tasks, what we would call observation protocol


### Observations protocol

- Silent observer

- Think aloud

- Constructive Interaction
Note:
	- There are three major of observation protols


### Interview
- Ask users  to give  you feedback


### Report
- Reporting results

- Summarize the experiences of users!

- Emphasize your insights 

- Offer suggestions for improvement 

Note:
- Lessons:
  - What part of a site or application are easy or hard to use
  - how usable is the site for each task
  - what improvement can be done to improve usability
- Emphasize your insights
	- Emphasize your insights with specific examples or quotes
- Offer suggestions for improvement
	- offer suggestions for improvement for tasks that were difficult to perform



### A/B Testing


- A/B Testing
	-  Not relying on Cognitive or psychological understanding of user's behavior

	- Present users with option A and B & measure the performance

- Good method to pickup best solution

Note: you can put these options live in your application and measure how well they perform.
 Allowing to find tremendous difference in performance based on very tiny changes in the way interface work

 - This can be done to measure which sorts of security features like authentication mechanism are more effective, but it can be used  in all kinds of ways.


### A/B Obama campaign website example1
![External Image](http://172.16.52.253:8000/plugin/markdown/img/chap3/obama1.png)


![External Image](http://172.16.52.253:8000/plugin/markdown/img/chap3/obama2.png)


![External Image](http://172.16.52.253:8000/plugin/markdown/img/chap3/obama3.png)


### A/B Obama campaign website example2
![External Image](http://172.16.52.253:8000/plugin/markdown/img/chap3/obamafamily.png)


![External Image](http://172.16.52.253:8000/plugin/markdown/img/chap3/obamachange.png)


![External Image](http://172.16.52.253:8000/plugin/markdown/img/chap3/obamaresult.png)


### A/B Twitter campaign example

![External Image](http://172.16.52.253:8000/plugin/markdown/img/chap3/twitter.png)


### Running A/B Test Details
- Start with a small percentage of visitors trying the experimental conditions

- Automatically stop testing if any condition has very bad performance

- Let people consistently see the same variation so they do not get  confused


### Conclusions
- Small tweaks in the Interface can lead to big difference in user behavior

- A/B Testing allows you to check  that by showing different versions of the site to people



### Case Study
- Goal:
	- Effectiveness of web browser phishing warning

Note: 
	When evaluation usability we often measure: speed, effiency ...
   but security  behaviour is not one of the list. So but how do we measure 
 	user security behaviour?
	That where efficiency plays. When we are measuring security behaviour 
	of a user we measure the effiency of the security features


