# Agenda
- Introduction
	- The Tradeoff between security & usability

- Human Computer Interaction 

- Usability

- Chunking Information

- Mental Model


## The Tradeoff Between Security and Usability


The doctor work station illustration
![External Image](http://172.16.52.247:8000/plugin/markdown/images/doctor.png)


- Issue with within the doctor system

- General considerations
	- Security and user experience should be co-developed

	- Start thinking of security and user computer system interaction from  the beginning

	- The mechanism of psychological acceptance may not always work

Note:
	- There should be a certain trade off between computer system usability and security
	system not designed with users in mind  may have their security by passed by users

- One of the principle of system security is the mechanism of psychological acceptance,
  when you try to persuade people to accept certain security measures even if they are counter productive

- There should be a certain  measures at which extend such as a mechanism can be applied and used


###  Meaning of Usability for computer system security
- Usability for computer security is about:
	- Understanding basics of  user computer interactions
	- Understanding of human's cognitive and psychologist abilities
	- How do people's task and what they are trying to do
	- Looking at method to design these facts into system & evaluating how the system do;



## Human-Computer interaction


- Human-computer interaction  or HCI, is the study of how people interact with technology
![External Image](http://172.16.52.247:8000/plugin/markdown/images/hci.png)


- Users may include:
    - Non mobile workers working with computers
    - Mobile workers

- In HCI is about understanding the people, the technology and how they fit together


### HCI from user's perspective
- It is interesting to understanding both the psychological and cognitive abilities of  users

- The fallout
  - Designing technology that take advantage of people's interabilities

  - Avoiding overtasking people by requiring them to do thing they are not capable off

Note:
- On overtasking people
    - technology should not be designed even for security reason to make people's work
    painful, difficult, otherwise this can result in having people bypassing the security
    measures for convenience.
    Good design should not extra, and un-necessary work for users


### HCI from technology perspective
- HCI allows to apprehend both design and evaluations of technology

- Technology is designed according first-hand knowledge


## Users, Tasks & Context
- Users
  - Anyone from children to adult
  - People working alone or people working in group
- Task
  - Things that are people are trying to accomplish within the the system such:
      - Login in, analyzing of data set so and so
- Context


## Why do they matter?
- Users, tasks and context's understanding is critical to good design practice
- Allowing to build technology that take into considerations our knowledge about users, their task and context
- Resulting in building user friendly technology system


## HCI: Evaluation
- Different than security evaluation
	- The goal here is not to evaluate the security of a system

	- But rather evaluating  system user friendly approach




## Usability


## Measuring usability
Factors to be considered when measuring usability:
  - Speed: How quickly can the task be accomplished
  - Efficiency: How many mistake are made in accomplishing the tasks
  - Learnability: How easy is it to learn to use the system
  - Memorability: Once learned, how easy it is to remember how to use the system
  - User preference: it's about what the users like the most

Note:
- A good system design may not include all of these metric in order to have good usability, there may be trade off somehow.

- Illustration:
	login to iPhone using password vs using touch ID


### How do we measure these metrics?
- Speed - timing
- Efficiency - counting errors


### Learnability measurement
![External Image](http://172.16.52.247:8000/plugin/markdown/images/learnmeas.png)


### Memorability Measurement
![External Image](http://172.16.52.247:8000/plugin/markdown/images/memorymeas.png)


![External Image](http://172.16.52.247:8000/plugin/markdown/images/memorymeas1.png)


## Tasks & Task analysis
- Analyzing the Usability of a system requires a set of task

- Task are goals that users set out to accomplish in a system

Note:


### Common errors in task creation
- Leading or too descriptive
- specific questions
  - what is the third headline on ccn.com?
- Directing users towards things you want to tell  them, not what they want to know


### Comparing tasks between systems
- To evaluate the usability of a system we can create a representative list of task
- evaluate the usability  of these tasks
- The illustration of file creation

Note:
Leading and too descriptive:
- Click on the username box at the upper  right of the screen and enter your username,
  then click on the password box underneath it and enter your password. Click submit
- specific questions
Tasks are too abstract to apprehend at first hand,
in order to test the usability of a system we need to create a set of task,
come up with specific goal that users wanna accomplish and analyzing the usability of those goal.
This will give a deep understanding about the system such where it sucks and work it  works



## Chunking Information


![External Image](http://172.16.52.247:8000/plugin/markdown/images/memory.png)
- Human brain has limitation in memory
- Understanding this in a cognitive perspective, allow the design of system that can take advantage of human capability


### Working memory
- Also called short term memory
- The part of system of interest when building system
- People can remember 7+/-2 things at the same (Georges A. Miller 1956)
- broadcast (1975): 4-6
- Le Compte(1999): 3
- Common practice:4+/-1

Note: if we design system we can keep in mind best solution


### Chunking:
- The mechanism of chuncking  helps understanding working memory


### Chunking: example 1
        - oomgydliev
        - old veg mi yo
        - video gym lo
        - I love my dog


### Chunking: Example 2
![External Image](http://172.16.52.247:8000/plugin/markdown/images/chunking1.png)


### The issue with password
- To easy password (vulnerable to brute force or dictionary attack)

- Complicated password rule may result into security issues


### Password Memory
- Create password with chunks
  Password creation can take advantage of the chunking memory
- Example: 08#11#71Lg12#11#81kd

- As per research result;
	- 50% of people will remember a 7 characters password
	- While 76% do remember 4-chunk password

Note: Picking up two dates
date items are delimited by special characters the followed by some initial
refering to LeCompte or Georges A miller theory this may be hard to memorize since it's 10 characters.



## Mental Model


![External Image](http://172.16.52.247:8000/plugin/markdown/images/mentalmodel.png)


### what is it about?
- an explanation of someone's thought process about how something works in the real world.
- a representation of the surrounding world, the relationships between its various parts
  and a person's intuitive perception about his or her own acts and their consequences
  (Wikipedia)
- It's an important part of Usability


### The use of previous experience
- To using a new system users do rely on mental model based:

  - on previous experiences

  - Previous system that they have been using

Note: user


### Mental Model Development
Factors playing into the development of mental model
- Affordance: Things that happens in a system showing users how they are supposed to be used	
- Constraint: Preventing doing things that should not be done in a system
- Conventions: Common understanding of what something means

Note:
Convention can be cultural or universal


## Affordance
- Mapping
- Visibility 
- Feedback


### Affordance: Mapping
Descripting how functionnalities matching something that you need
![External Image](http://172.16.52.247:8000/plugin/markdown/images/mapping.png)

Note:
which nugg control which burner? Not a really good mapping 


![External Image](http://172.16.52.247:8000/plugin/markdown/images/mapping1.png)

Note:
which nugg control which burner? a better mapping


![External Image](http://172.16.52.247:8000/plugin/markdown/images/mapping2.png)


![External Image](http://172.16.52.247:8000/plugin/markdown/images/mapping3.png)


### Affordance: Visibility
![External Image](http://172.16.52.247:8000/plugin/markdown/images/visibility.png)

Note: How easy  you can see most important functionality


### Affordance: Feedback


## Constrain


### Constrain
![External Image](http://172.16.52.247:8000/plugin/markdown/images/constrain.png)


![External Image](http://172.16.52.247:8000/plugin/markdown/images/constrain1.png)


![External Image](http://172.16.52.247:8000/plugin/markdown/images/constrain2.png)


![External Image](http://172.16.52.247:8000/plugin/markdown/images/constrain3.png)


![External Image](http://172.16.52.247:8000/plugin/markdown/images/constrain4.png)


### Convention
![External Image](http://172.16.52.247:8000/plugin/markdown/images/convention.png)
