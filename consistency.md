## The Influence
					COMMITMENT AND CONSISTENCY



## Agenda
- Consistency
	- The Principle of consistency
	- Illustration of consistency
- Commitment
	- The principle of commitment
	- Procedures to create commitment
	- Effectiveness of commitment
	- The Inner Choice
	- Useful techniques to obtain commitment



## Consistency
					The principle of consistency


### Consistency: What is it?
- Conformity in the application of something, typically that which is necessary for the sake of logic, accuracy, or fairness (Oxford dictionary)
- Our nearly obsessive desire to be consistent with what we have done;
- The willingness to believe in correctness even if difficult choice has been made
- Like other weapons of influence
	- consistency lies deep within us;
	- Consistency directing our actions with quiet power

Note:	Explanation and details
- Consistency is the kind of trait that we desire for personal requirements
or through system that we do build

- Indeed, we all fool ourselves from time to time in order to keep thoughts
and beliefs consistent with what we have already done or decided

- Prominent theorists such as Leon Festinger, Fritz Hieder, and
Theodore Newcomb have viewed the desire for consistency as a central motivator of our behavior


### Consistency: Illustration
- The  horse race betting consistency's illustration	

- New York City beach  thefts staging illustration

	- When subjects are a not addressed, they are reluctant, 4/20 take action to stop the theft

	- When subjects are addressed, propelled by the rule for consistency 19/20 take actions

Note:
- The horse race betting illustration:
	A STUDY DONE BY A PAIR OF CANADIAN PSYCHOLOGISTS UNCOVERED something
	fascinating about people at the racetrack: Just after placing a bet, they are much
	more confident of their horse’s chances of winning than they are immediately
	before laying down that bet.1 Of course, nothing about the horse’s chances actually
	shifts; it’s the same horse, on the same track, in the same field; but in the minds of
	those bettors, its prospects improve significantly once that ticket is purchased.

- New York City beach  thefts staging:


### Automatic consistency: The art of our comfort zone
- Automatic consistency can supply a safe hiding place

- Sealed within the fortress walls of rigid consistency, we can be impervious to the sieges of reason.

- The Transcendental mediation's illustration

Note:
	Sometimes it is not the effort of hard, cognitive work that makes us shirk thoughtful
	activity, but the harsh consequences of that activity. Sometimes it is the cursedly
	clear and unwelcome set of answers provided by straight thinking that makes us
	mental slackers. There are certain disturbing things we simply would rather not
	realize. Because it is a preprogrammed and mindless method of responding,
	automatic consistency can supply a safe hiding place from those troubling
	realizations. Sealed within the fortress walls of rigid consistency, we can be
	impervious to the sieges of reason.


### Consistency: Importance
![External Image](http://172.16.52.249:8000/plugin/markdown/images/inconistency.png)


### Consistency: Importance
- Why does consistency is so important?

	- In most circumstances consistency is valued and adaptive

	- Its opposite, inconsistency is commonly thought to be and undesirable personal trait

	- Good Personal consistency is highly valued in our culture


### Our vulnerability to consistency
- Often causing us to act in ways that are clearly contrary to our own best interest
- When it occurs unthinkingly, in an automatic way, consistency can be disastrous
- Like other forms of automatic responding, it offers shortcut through the density of modern life
- Automatic consistency functions as a shield against thought and thus being exploitable.
- The drive to be consistent constitutes a  highly potent weapon of social influence
- The seasonal sale issues illustration



## Commitment
- Context: Consistency is formidable in directing human action;

![External Image](http://172.16.52.249:8000/plugin/markdown/images/commitment.png)

	But, how can we get  this powerful consistency tapes activated?


### Commitment: What is it?
- An engagement or obligation that restricts freedom of action, the state or quality of being dedicated to a cause, activity;
		- Responsibility, obligation, duty, burden
		- liability, pressure, dedication, loyalty

- There is a connection between commitment and consistency

- Commitment strategies are aimed at us by compliance professionals

Note: Each of these strategies is intended to get us to take some action or make some statement that will trap  us later into compliance through consistency pressures.


### Procedures to create commitment
- They may take various form

- Some are straightforward; others are among the most subtle compliance tactics

- The door-to-door donations collections illustration


### Procedures to create commitment: illustration
- The telephone solicitors for charity crafty technic illustration

	- The he psychologist Steven J. Sherman called sample of Bloogmington, Indiana, residents

	- Asked  for reaction IF asked to spend 3 hours collecting money for America Cancer Society (looking for small commitment)

	- Getting representative from ACS to call few days later looking for  volunteers

Note:
For instance you want to increase the number of people
in your aread who would agree to go door-to-door
collecting donations for your favorite charity.

- Use the approach used by the psychologist Steven J. Sherman
	- Calling sample of Bloogmington, Indiana, residents as
 	 part of a survey he was taking and asked them to predict
	 they WOULD say if asked to spend three hours collecting money for America Cancer Society (looking for small commitment)
- At the same fashion the solicitors could have asked during the process about health and well being. The fact is not because
they are polite or are looking for your well being, but they are looking for you to day you are fine. Once you state this
publicly, it becomes much easier for them to ask you to contribute with donation to help out unfortunate victim


### Effectiveness of  commitment

- What makes a commitment effective?

	- The chinese POW  illustration
		- leniency policy
			- Concerted and sophisticated psychological assault on captive
		- Start small and build

	- The foot-in-door technique

		- start with little request in order to gain eventual compliance with larger request
- Not all commitment affect self-image. There are certain required conditions for commitment to be effective

- Tips:  Use small commitments to manipulate a person's self-image

Note:
- During the Korean war many American soldier found themselves as POC camps run by Chinese
- Chinese in the opposite of their counterpart North Koreans, used the lenient policy
- The Chinese heavily relied on commitment and consistency pressure to gain the desired compliance from
prisoners
- Problem faced by Chinese: how to get any collaboration at from men who where trained to provide nothing byt name name, rank and serial number?
Answer was sample: start small  and build (foot in the door principle)

Tactic used:
- Prisoners were frequently asked to make so mildly (without anger) to anti American or pro-Communist such
   - US is not perfect. In communist country unemployment is not a problem (small request)
- Push for bigger request
   - A man who stated that the US is not perfect might then be asked to describe in which way.
   - Once the man explained himself, he then asked for to make a list of " problems with American and sigh his name on it
   - Later he might then be asked to read them in discussion group with other prisoners
   - The Chinese then might use his name  and his essay in anti American radio broadcast
   - Aware that he had written the essay the essay without any strong thread or coercion, many times a man
would change his image of himself to be consistent with the deed and with the new collaborator.




## Usefull tactiques to get commitment
- The Magic Act
- The Public Eye
- The Effort Extra
- The Inner Choice


### The magic act
- People's true feeling & belief come less from their words than from their deed
	- The principle of self perception
	- The principle of written statement:
			Providing physical  evidence that the act occurred;
			Given a mechanism to influence other's thinking
			with one's idea;
	- The influence of active commitment on consistency
			1- From the inside, there is pressure to bring
			self image into line  with action
			2- from the outside: sneakier pressure-> a tendency
			to adjust this image according to they way others perceive us.
Note:
- The influence's of active commitment on consistency:
	Once active commitment is made, self-image is
	squezed from two sides by consistency pressure


#### The principle of written statement in business world
- Compliance professionals also know about the committing power of written statement

- The door-to-door sales companies illustrations
	- Context: The coolling-off laws
	- Getting customers themselves to write to fill out sales agreement
	- Resulting in reducing the number of sales cancellation

Note:
- The principle of written statement
	- Once a man wrote what the Chinese wanted, it was very difficult for him to believe he had not done so.
	- The opportunities to forget or to deny to himself what he had done where not available as they are for purely verbal statements


### The public eye
- The public commitment principle
	- Public commitment tend to be lasting commitments

	- Whenever ones takes stand  that is visible to others, there arises a drive:
			To maintain that stand in order  look like
			a consistent person

			For appearance's sake, the more public a stand,
			the more reluctant we will be to change it.
- Illustration

Note:
- public commitment principle
	- To continue with example of the Chinese POW: the Chinese constantly arranged to have pro-communist statement of their captives seen by others
- Illustration:
	- According to Deutcsch and Gerard findings  that we are truest to our decisions if we have bound ourselves to them publicly can 		 be put to good use.
- The weight-loss:  
	Weight-reduction clinics will require  down an immediate weigh loss goal and show that goal to as many friends, relatives and neighbors possible
- Woman stop smoking:


### The effort extra
- The more effort principle:
	- The more effort into a commitment, the great is its ability to influence attitude


- The hell week illustration

Note:


### The inner choice
- We've seen that:
	- Commitment are most effective when active, public & effortful
- Inner choice is the most important property of effective commitment
- Illustration with Chinese POW
- The hell week fraternity  illustration
- The  Freedman experimentation
Note:
Social scientists have determined that we accept inner responsibility for a
behavior when we think we have chosen to perform it in the absence of strong
outside pressures. A large reward is one such external pressure. It may get us to
perform a certain action, but it won’t get us to accept inner responsibility for the act.
Consequently, we won’t feel committed to it. The same is true of a strong threat; it
may motivate immediate compliance, but it is unlikely to produce long-term
commitment.


### vulnerability & Explanation
- Commitment with inner choice:
	- change from inner choice is not specific to  a given situation but covers a whole range of situations
	- its effect last longer
	- Commitment that leads to inner change grow their own legs
	- The lowball principle:

	Note:
	The advantage to an unscrupulous compliance professional is tremendous.
	Because we build new struts to undergird choices we have committed ourselves to,
	an exploitative individual can offer us an inducement for making such a choice, and
	after the decision has been made, can remove that inducement, knowing that our
	decision will probably stand on its own newly created legs.


### The lowball principle: The car dealer illustration
- Very good price is offered  on a car (often bellow the competition)
- Getting you to make decision on purchasing the new car
	- Filling of raft of purchase
	- extensive financing terms
	- Encouraging test driving
- Then calculation error occurs


Note:
- The dealership   example:
- The energy saving principle


### HOW TO SAY NO?
-  A foolish consistency is the hobgoblin of little minds
-  Consistency and commitment are general good
- But automatic and unthinkable consistency may be bad
