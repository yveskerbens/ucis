# Negotiation - Chapter 2:

## Litterature

- Successful Negotiation Essential Strategies and Skills, Siedel 

## Structure

- Other side
- Power of negotiation
- Psychological tools

# Other side

## General view

- Difficult to  get to know the other side first
	- i.e.: American time oriented
		![Time is money](https://velzblog.files.wordpress.com/2018/11/75b28c7d-920d-4ea9-9b99-d37a46ff7dcd.png)
	- Want to start negotiate the deal directly


## Example

- Lawyer from Singapore
- Negotiation for free trade agreement
- Indian want to know the people as individual
	- Need to trust the other side
- Negotiation failed

## Example con't

- Cross cultural negotiation training
- Went back and succeeded the deal

## Example con't

- Paris - La Defense
- Diner with the president of new university
- Talk about everything but NOT the deal
	- William Blake
- Day after
	- Half day programmed for negotiation
	- School directly gave them the space to teach
	- No rent, just a part of profits

## Advices

- Avoid your favourite topic
	- You will talk too much
- Ask a lot of questions
	- People love to talk about themselves

## Advices - con't

![listen is crutial](https://measuringsel.casel.org/wp-content/uploads/2017/09/voice.listen.png)

## Conclusion

- Important to know the other side
	- Very important in some culture
		- They prefer to evaluate the quality of the other side
		- Based on trust

# Power of negotiation

## Source

- Information
	- Ask a lot of questions
		- To analyse the position and interests
		- Important to listen
			- 2 ears and 1 mouth to listen more than we speak
	- BATNA

## Source of power

- Two factors
	- Conceptual knowledge
		- Experience in the business
		- Understanding the business
		- Building on your experience
	- Ability to listen

Note:
- No courses "listening" at school
- Very important


## Source of power

- BATNA
- Should we disclose our BATNA ?
	- If weak alternative : NO
	- If strong alternative : YES

## Example - Ford

- Ford negotiate with a supplier
	- Directly tell their BATNA
		- Want to cut your cost
		- If not possible we have 5 others waiting


## Example - Ford

- Response from supplier could be :
	- It's been x years
	- Always on time
	- Good quality
	- R&D followed by us

## Strategy

- BATNA
	- How powerful are they ? (find their BATNA)
	- Weaken their power (their BATNA)
	- Improve your power (your BATNA)

## Strategy

- But BATNA does not work all the time
	- Coalition (50%-30%-20%)
		- Need trust

## Conclusion

- Understand, experience the business
- Ask a lot of question
	- Listen
	- Avoid your favourite topic
	- Trust from the other side
- BATNA

# Psychological tools

##  Heuristics
- Simplify the negotiation
	- i.e. : Recruits only people from top 10 universities

Note:
- Pre-screened to get the best
- But another one could have better experiences and not there

## Tricks and Traps - Plan

- Mythical fixed pie assumption
- Anchoring
- Overconfidence
- Framing
- Availability
- Escalation
- Reciprocation
- Contrast principle
- Big-picture perspective


## Mythical fixed pie assumption  

- "Assuming that there is a conflict of interests"
- Example :
	- Arm wrestling match result
	- They juste wanted to score a lot
	- Not a competition, not a competitor
- Called reactive devaluaiton
	- ex: arm reduction proposal
		- USA president
		- Gorbatchev

## Anchoring

- "Anchor on an initial value when estimating the value of uncertain objects"

![](https://miro.medium.com/max/300/1*8QVwnNYqtCvcCfWGHKRXRA.png)

## Anchoring - Example

-  Experiment with doctors
	- Probability of lung diseases

## Overconfidence

- "We are overconfident that our judgements are corrects"
- We are overconfident
	- ie: exepiment with range
		- We choose a too short range

## Consequences

- Consequence for finance
	- We trade too much
	- Loose more money than we should
- Consequence for managers
	- If good at short-term decision
	- Often bad at long term


## Consequences - con't

- Nogotiation
	- ZOPA (Zone of potential agreement)
		- Zone is too narrow
- Can be good for
	- To persuade people to make more

## Framing

- "The way that options are framed cause us averse to risk"
- Positive choice
	- Going to the sure thing
- Negative choice  
	- Going to the unsure thing

## Example

- Warning the way the choices are framed
	- Same choice can be formulated differently
		- 200/600 people will be saved
		- 400/600 people will die


## Availability

- "We are influenced by information that is most easily available"
- We are more exposed to certain information than other
	- Dramatic videos are more easily retained

## Escalation

- "The most negotiators look at negotiations from the perspective of the other side"

![](https://cdn3.vectorstock.com/i/1000x1000/40/52/businessman-holding-money-for-auction-bidding-vector-13744052.jpg)

## Escalation - Con't

- Competition force people to escalate
	- Too competitive = meaningless decisions
	- i.e.: $ auction
	- Called Competitive Arousal

## Competitive Arousal

- Intense rivalry
- Time pressure
- In the spotlight

## Solution

- Limit the role of someone
- Manage time better
- Spread responsibility (no spotlight)
- Look the deal from the other side

## Reciprocation

- "We feel the need to repay what someone has given us"
	- Typically human being

[![Alt](http://img.youtube.com/vi/oy_wU0NpzVY/0.jpg)](http://www.youtube.com/watch?v=oy_wU0NpzVY)


Note : 	
- Begin with share food, tools ...


## Example

- Offer a gift to someone not interested
- Give information about something
- At the end we give back something ...

## Contrast principle

- "Things look different when presented in sequence"

![](https://i1.wp.com/www.girlschase.com/media/2019/06/contrast-principle-1.jpg?w=1200&ssl=1)


## Example
- Real estate 	
	- Setup house
		- Huge price, lots of work to do
	- Then the good one
		- Big constrat with the first one

## Big picture perspective

- "Do not get lost into details"
- Always keep in mind the big picture
	- To void traps and being lost in the details

## Conclusion

- Always keep in mind the traps
- Always think if it is not a trick used to sell
