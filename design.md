Design

What is Design
Design is really two different of things
it's process  or Designing functionalities and visual look

How does the design process work and how does it improve usability?
so most of participatory design

Including your target people earlier in the process
if you ware going for children you design with children

Participatory design  vs co-design

Story design and co-design process

What is the best way to incorporate design methodology into our work?
Design goal:

What if you are just some guy building an app your basement with limited resources? What's the best way to do design methods?
Who are you building the app for?
Bring people earlier in the process?


What's the hardest party of design?
Hardest part is there no right answers, there is  good direction, can't do it that way


Think of constraints  these can motivate your creativity and make us more innovative


Design methodologies
1- Design process. The goal is:
- Where do ideas come from
- Many processes:
    - Iterative design
    - System centered Design
    - user centered design
    - Participatory design
    - Designer centered design


Iterative Design
    - Requirements
    - Design
    - Testing
    - Development

System centered centered Designer
  - what can be built  easily on this platform?
  - What can I create from the available tools?
  - What do I as a programmer find interesting to work on?


User Centered Design
Design  is based upon a user's
  - Abilities and real needs
  - context
  - works
  - Tasks


Golden rule of interface design
- Know the users


Participatory Design
. Problem
    - intuition wrong
    - interviews etc. not precise
    - designer cannot know the user sufficiently well to answer all issues that come up during the design
. Solution
    - designers should have access to pool of representative users. That is, END users, not their managers or union reps!

  In participatory you involve the group of people  that will use the product that are  you  designing

  Brain storming

Designer centered design



Users  can give a lot of valuable insight for design
  - Tasks
  - Context
  - Needs

Support designers coming up with ideas
iterate to build better systems


Example:
Usability of Firefox's untrusted connection error



Incorrect computer clock setup may cause SSL certificate to look like as being expired
